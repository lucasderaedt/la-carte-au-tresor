package Main;

public class Treasure {

    private int[] coordinate;
    private int quantity;

    public Treasure(int[] coordinate, int quantity) {
        this.coordinate = coordinate;
        this.quantity = quantity;
    }

    public Treasure() {
    }

    public void setCoordinate(int[] coordinate) {
        this.coordinate = coordinate;
    }

    public int[] getCoordinate() {
        return coordinate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Main.Treasure{" +
                "coordinate=" + coordinate +
                ", quantity=" + quantity +
                '}';
    }
}

