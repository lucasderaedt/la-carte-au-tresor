package Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class Adventurer {

    private String name;
    private int[] coordinate;
    private char[] instructions;
    private String orientation;
    private int treasure = 0;
    private final List<String> orientationOrder = new ArrayList<>(){ //this the list of possible orientation in the right order
        {
            add("N");
            add("E");
            add("S");
            add("O");
        }
    };

    public Adventurer(String name, int[] coordinate, char[] instructions, String orientation, int treasure) {
        this.name = name;
        this.coordinate = coordinate;
        this.instructions = instructions;
        this.orientation = orientation;
        this.treasure = treasure;
    }

    public Adventurer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(int[] coordinate) {
        this.coordinate = coordinate;
    }

    public char[] getInstructions() {
        return instructions;
    }

    public void setInstructions(char[] instructions) {
        this.instructions = instructions;
    }

    public List<String> getOrientationOrder() {
        return orientationOrder;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public int getTreasure() {
        return treasure;
    }

    public void setTreasure(int treasure) {
        this.treasure = treasure;
    }

    public void turnToTheRight(){ //function that allows the adventurer to change of orientation by turning him of 90° to the right

        OptionalInt integer = IntStream.range(0, orientationOrder.size()).filter(i -> this.orientation.equals(orientationOrder.get(i))).findFirst();

        if (integer.isPresent()){
            if (integer.getAsInt() == 3){
                this.orientation = orientationOrder.get(0);
            }else{
                this.orientation = orientationOrder.get(integer.getAsInt()+1);
            }
        }

    }

    public void turnToTheLeft(){ //function that allows the adventurer to change of orientation turning him of 90° to the left

        OptionalInt integer = IntStream.range(0, orientationOrder.size()).filter(i -> this.orientation.equals(orientationOrder.get(i))).findFirst();

        if (integer.isPresent()){
            if (integer.getAsInt() == 0){
                this.orientation = orientationOrder.get(3);
            }else{
                this.orientation = orientationOrder.get(integer.getAsInt()-1);
            }
        }

    }

    @Override
    public String toString() {
        return "Main.Adventurer{" +
                "name='" + name + '\'' +
                ", coordonate=" + coordinate +
                ", instructions='" + instructions + '\'' +
                ", orientation='" + orientation + '\'' +
                '}';
    }
}
