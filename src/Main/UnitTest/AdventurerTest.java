package Main.UnitTest;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class AdventurerTest {

    private final List<String> orientationOrder = new ArrayList<>(){ //this the list of possible orientation in the right order
        {
            add("N");
            add("E");
            add("S");
            add("O");
        }
    };

    @Test
    void turnToTheRight() { //test with a simple increment

        //arrange
        String reference = "O";
        String entrance = "S";
        String valueAfterTreatment = "";

        //act
        OptionalInt integer = IntStream.range(0, orientationOrder.size()).filter(i -> entrance.equals(orientationOrder.get(i))).findFirst();

        if (integer.isPresent()){
            if (integer.getAsInt() == 3){
                valueAfterTreatment = orientationOrder.get(0);
            }else{
                valueAfterTreatment = orientationOrder.get(integer.getAsInt()+1);
            }
        }

        //assert
        assertEquals(reference, valueAfterTreatment);

    }

    @Test
    void turnToTheRightEndList() { // test with the increment at the end of the list's index

        //arrange
        String reference = "N";
        String entrance = "O";
        String valueAfterTreatment = "";

        //act
        OptionalInt integer = IntStream.range(0, orientationOrder.size()).filter(i -> entrance.equals(orientationOrder.get(i))).findFirst();

        if (integer.isPresent()){
            if (integer.getAsInt() == 3){
                valueAfterTreatment = orientationOrder.get(0);
            }else{
                valueAfterTreatment = orientationOrder.get(integer.getAsInt()+1);
            }
        }

        //assert
        assertEquals(reference, valueAfterTreatment);

    }

    @Test
    void turnToTheLeft() { //test with a simple decrement

        //arrange
        String reference = "E";
        String entrance = "S";
        String valueAfterTreatment = "";

        //act
        OptionalInt integer = IntStream.range(0, orientationOrder.size()).filter(i -> entrance.equals(orientationOrder.get(i))).findFirst();

        if (integer.isPresent()){
            if (integer.getAsInt() == 0){
                valueAfterTreatment = orientationOrder.get(3);
            }else{
                valueAfterTreatment = orientationOrder.get(integer.getAsInt()-1);
            }
        }

        //assert
        assertEquals(reference, valueAfterTreatment);

    }

    @Test
    void turnToTheLeftStartList() { //test with a decrement at the start of list's index

        //arrange
        String reference = "O";
        String entrance = "N";
        String valueAfterTreatment = "";

        //act
        OptionalInt integer = IntStream.range(0, orientationOrder.size()).filter(i -> entrance.equals(orientationOrder.get(i))).findFirst();

        if (integer.isPresent()){
            if (integer.getAsInt() == 0){
                valueAfterTreatment = orientationOrder.get(3);
            }else{
                valueAfterTreatment = orientationOrder.get(integer.getAsInt()-1);
            }
        }

        //assert
        assertEquals(reference, valueAfterTreatment);

    }
}