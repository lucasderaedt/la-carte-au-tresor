package Main.UnitTest;

import Main.Adventurer;
import Main.MapTempo;
import Main.Treasure;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MapTempoTest {

    @Test
    void isItAvailableWithMountain() { //test for a moove in coordinate where there is a mountain

        //arrange
        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> moutainsList = new ArrayList<>();
        moutainsList.add(new int[]{
                1,1
        });
        mapTempo.setMoutainsList(moutainsList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurer = new Adventurer("Lara",
                new int[]{1,2}, new char[]{'A'}, "N", 0);
        adventurersList.add(adventurer);
        mapTempo.setAdventurersList(adventurersList);

        List<Treasure> treasuresList = new ArrayList<>();
        Treasure treasure = new Treasure(new int[]{1,3}, 2);
        treasuresList.add(treasure);
        mapTempo.setTreasuresList(treasuresList);

        //parameter
        int[] coordinate = new int[]{
                1,1
        };
        boolean flag = true;

        //act
        if(coordinate[0] < 0 || coordinate[1] < 0){
            flag = false;
        }else if (coordinate[0] > mapTempo.getWidth()-1 || coordinate[1] > mapTempo.getHeight()-1){
            flag = false;
        }

        if(flag){
            for (int[] tab : moutainsList){
                if (tab[0] == coordinate[0] && tab[1] == coordinate[1]) {
                    flag = false;
                    break;
                }
            }
        }


        for (Adventurer tempo : adventurersList){
            if (tempo.getCoordinate()[0] == coordinate[0] && tempo.getCoordinate()[1] == coordinate[1]){
                flag = false;
                break;
            }
        }

        //assert
        assertFalse(flag);

    }

    @Test
    void isItAvailable() { //test for a moove in coordinate where there is nothing

        //arrange
        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> moutainsList = new ArrayList<>();
        moutainsList.add(new int[]{
                2,3
        });
        mapTempo.setMoutainsList(moutainsList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurer = new Adventurer("Lara",
                new int[]{1,2}, new char[]{'A'}, "S", 0);
        adventurersList.add(adventurer);
        mapTempo.setAdventurersList(adventurersList);

        List<Treasure> treasuresList = new ArrayList<>();
        Treasure treasure = new Treasure(new int[]{1,3}, 2);
        treasuresList.add(treasure);
        mapTempo.setTreasuresList(treasuresList);

        //parameter
        int[] coordinate = new int[]{
                2,2
        };
        boolean flag = true;

        //act
        if(coordinate[0] < 0 || coordinate[1] < 0){
            flag = false;
        }else if (coordinate[0] > mapTempo.getWidth()-1 || coordinate[1] > mapTempo.getHeight()-1){
            flag = false;
        }

        if(flag){
            for (int[] tab : moutainsList){
                if (tab[0] == coordinate[0] && tab[1] == coordinate[1]) {
                    flag = false;
                    break;
                }
            }
        }


        for (Adventurer tempo : adventurersList){
            if (tempo.getCoordinate()[0] == coordinate[0] && tempo.getCoordinate()[1] == coordinate[1]){
                flag = false;
                break;
            }
        }

        //assert
        assertTrue(flag);

    }

    @Test
    void isItAvailableWithGoingAway() { //test for a moove out of the width of the map

        //arrange
        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> moutainsList = new ArrayList<>();
        moutainsList.add(new int[]{
                1,1
        });
        mapTempo.setMoutainsList(moutainsList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurer = new Adventurer("Lara",
                new int[]{1,2}, new char[]{'A'}, "N", 0);
        adventurersList.add(adventurer);
        mapTempo.setAdventurersList(adventurersList);

        List<Treasure> treasuresList = new ArrayList<>();
        Treasure treasure = new Treasure(new int[]{1,3}, 2);
        treasuresList.add(treasure);
        mapTempo.setTreasuresList(treasuresList);

        int[] coordinate = new int[]{
                3,1
        };
        boolean flag = true;

        //act
        if(coordinate[0] < 0 || coordinate[1] < 0){
            flag = false;
        }else if (coordinate[0] > mapTempo.getWidth()-1 || coordinate[1] > mapTempo.getHeight()-1){
            flag = false;
        }

        if(flag){
            for (int[] tab : moutainsList){
                if (tab[0] == coordinate[0] && tab[1] == coordinate[1]) {
                    flag = false;
                    break;
                }
            }
        }


        for (Adventurer tempo : adventurersList){
            if (tempo.getCoordinate()[0] == coordinate[0] && tempo.getCoordinate()[1] == coordinate[1]){
                flag = false;
                break;
            }
        }

        //assert
        assertFalse(flag);

    }

    @Test
    void isThereATreasure() { //when there is a treasure

        //arrange
        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> moutainsList = new ArrayList<>();
        moutainsList.add(new int[]{
                1,1
        });
        mapTempo.setMoutainsList(moutainsList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurer = new Adventurer("Lara",
                new int[]{1,2}, new char[]{'A'}, "N", 0);
        adventurersList.add(adventurer);
        mapTempo.setAdventurersList(adventurersList);

        List<Treasure> treasuresList = new ArrayList<>();
        Treasure treasureTempo = new Treasure(new int[]{1,3}, 2);
        treasuresList.add(treasureTempo);
        mapTempo.setTreasuresList(treasuresList);

        int[] coordinate = new int[]{
                1,3
        };
        boolean flag = true;

        //act
        for (Treasure treasure : mapTempo.getTreasuresList()){
            if (treasure.getCoordinate()[0] == coordinate[0]
                    && treasure.getCoordinate()[1] == coordinate[1]
                    && treasure.getQuantity() > 0){
                treasure.setQuantity(treasure.getQuantity()-1);
                flag = true;
            }else{
                flag = false;
            }
        }

        //assert
        assertTrue(flag);

    }

    @Test
    void isThereATreasureWithoutTreasure() { //when there is no treasure

        //arrange
        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> moutainsList = new ArrayList<>();
        moutainsList.add(new int[]{
                1,1
        });
        mapTempo.setMoutainsList(moutainsList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurer = new Adventurer("Lara",
                new int[]{1,2}, new char[]{'A'}, "N", 0);
        adventurersList.add(adventurer);
        mapTempo.setAdventurersList(adventurersList);

        List<Treasure> treasuresList = new ArrayList<>();
        Treasure treasureTempo = new Treasure(new int[]{1,3}, 2);
        treasuresList.add(treasureTempo);
        mapTempo.setTreasuresList(treasuresList);

        int[] coordinate = new int[]{
                2,3
        };
        boolean flag = true;

        //act
        for (Treasure treasure : mapTempo.getTreasuresList()){
            if (treasure.getCoordinate()[0] == coordinate[0]
                    && treasure.getCoordinate()[1] == coordinate[1]
                    && treasure.getQuantity() > 0){
                treasure.setQuantity(treasure.getQuantity()-1);
                flag = true;
            }else{
                flag = false;
            }
        }

        //assert
        assertFalse(flag);

    }

    @Test
    void exportMap(){ //checks if the export file is corresponding to what we want

        //Arrange
        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> moutainsList = new ArrayList<>();
        moutainsList.add(new int[]{
                1,1
        });
        mapTempo.setMoutainsList(moutainsList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurerTempo = new Adventurer("Lara",
                new int[]{1,2}, new char[]{'A'}, "N", 0);
        adventurersList.add(adventurerTempo);
        mapTempo.setAdventurersList(adventurersList);

        List<Treasure> treasuresList = new ArrayList<>();
        Treasure treasureTempo = new Treasure(new int[]{1,3}, 2);
        treasuresList.add(treasureTempo);
        mapTempo.setTreasuresList(treasuresList);

        List<String> exportFile = new ArrayList<>();
        exportFile.add("C - 3 - 4");
        exportFile.add("M - 1 - 1");
        exportFile.add("T - 1 - 3 - 2");
        exportFile.add("A - Lara - 1 - 2 - N - 0");

        //Act
        List<String> exportMap = new ArrayList<>();
        exportMap.add("C - " + mapTempo.getWidth() + " - " + mapTempo.getHeight());

        for (int[] mountain : mapTempo.getMoutainsList()){
            exportMap.add("M - " + mountain[0] + " - " +
                    mountain[1]);
        }

        for (Treasure treasure : mapTempo.getTreasuresList()){
            if (treasure.getQuantity()!=0){
                exportMap.add("T - " + treasure.getCoordinate()[0] + " - " +
                        treasure.getCoordinate()[1] + " - " +
                        treasure.getQuantity());
            }

        }

        for (Adventurer adventurer : mapTempo.getAdventurersList()){
            exportMap.add("A - " + adventurer.getName() + " - " +
                    adventurer.getCoordinate()[0] + " - " +
                    adventurer.getCoordinate()[1] + " - " +
                    adventurer.getOrientation() + " - " +
                    adventurer.getTreasure());
        }

        assertEquals(exportFile, exportMap);
    }
}