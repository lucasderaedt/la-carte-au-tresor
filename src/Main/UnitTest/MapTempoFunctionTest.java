package Main.UnitTest;

import Main.Adventurer;
import Main.MapTempo;
import Main.Treasure;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;


import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MapTempoFunctionTest {

    //@BeforeAll
    //

    @Test
    void getCoordinate() {

        //Arrange
        String[] line = new String[]{
                "M",
                "1",
                "2"
        };

        //Act
        int[] coordinate = new int[]{
                Integer.parseInt(line[1]),
                Integer.parseInt(line[2]),
        };

        //Assert
        assertEquals(1, coordinate[0]);
        assertEquals(2, coordinate[1]);

    }

    @Test
    void mapTempoCreation() {

        //Arrange
        List<String> input = new ArrayList<>();
        input.add("C - 3 - 4");
        input.add("M - 1 - 1");
        input.add("T - 1 - 2 - 2");
        input.add("A - Lara - 1 - 1 - S - AADADAGGA");

        MapTempo reference = new MapTempo();
        reference.setHeight(4);
        reference.setWidth(3);

        List<int[]> mountainsList = new ArrayList<>();
        mountainsList.add(new int[]{
                1,1
        });
        reference.setMoutainsList(mountainsList);

        List<Treasure> treasureList = new ArrayList<>();
        Treasure treasureTempo = new Treasure();
        treasureTempo.setCoordinate(new int[]{
                1,2
        });
        treasureTempo.setQuantity(2);
        treasureList.add(treasureTempo);
        reference.setTreasuresList(treasureList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurerTempo = new Adventurer();
        adventurerTempo.setCoordinate(new int[]{
                1,1
        });
        adventurerTempo.setName("Lara");
        adventurerTempo.setInstructions("AADADAGGA".toCharArray());
        adventurerTempo.setOrientation("S");
        adventurersList.add(adventurerTempo);
        reference.setAdventurersList(adventurersList);

        //Act
        MapTempo mapTempo = new MapTempo();

        for (String line : input){

            String[] tab = line.split(" - "); //splits the raw line to get only the values that are interesting for us

            switch (tab[0]) {
                case "C" -> { // if it's the map
                    mapTempo.setWidth(Integer.parseInt(tab[1]));
                    mapTempo.setHeight(Integer.parseInt(tab[2]));
                }

                case "M" -> { //if it's a mountain
                    mapTempo.getMoutainsList().add(new int[]{
                            Integer.parseInt(tab[1]),
                            Integer.parseInt(tab[2])
                    });
                }

                case "T" -> { //if it's a treasure
                    Treasure treasure = new Treasure();
                    treasure.setCoordinate(new int[]{
                            Integer.parseInt(tab[1]),
                            Integer.parseInt(tab[2])
                    });
                    treasure.setQuantity(Integer.parseInt(tab[3]));
                    mapTempo.getTreasuresList().add(treasure);
                }

                case "A" -> { //if it's an adventurer
                    Adventurer adventurer = new Adventurer();
                    adventurer.setCoordinate(new int[]{
                            Integer.parseInt(tab[2]),
                            Integer.parseInt(tab[3])
                    });
                    adventurer.setName(tab[1]);
                    adventurer.setOrientation(tab[4]);
                    adventurer.setInstructions(tab[5].toCharArray());
                    mapTempo.getAdventurersList().add(adventurer);
                }
            }

        }

        //Assert
        //assertEquals(reference, mapTempo);
        assertThat(reference)
                .usingRecursiveComparison()
                .isEqualTo(mapTempo);

    }

    private MapTempo initializeMapTempo(){

        MapTempo mapTempo = new MapTempo();
        mapTempo.setWidth(3);
        mapTempo.setHeight(4);

        List<int[]> mountainList = new ArrayList<>();
        mountainList.add(new int[]{
                1,1
        });
        mapTempo.setMoutainsList(mountainList);

        List<Treasure> treasureList = new ArrayList<>();
        Treasure treasure = new Treasure();
        treasure.setCoordinate(new int[]{
                2,0
        });
        treasure.setQuantity(2);
        treasureList.add(treasure);
        mapTempo.setTreasuresList(treasureList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurerTempo = new Adventurer();
        adventurerTempo.setCoordinate(new int[]{
                1,0
        });
        adventurerTempo.setName("Lara");
        adventurerTempo.setInstructions("AGA".toCharArray());
        adventurerTempo.setOrientation("S");
        //adventurerTempo.setTreasure();
        adventurersList.add(adventurerTempo);
        mapTempo.setAdventurersList(adventurersList);

        return mapTempo;
    }

    @Test
    void exploration() { //checks if the movements of adventurers follow their instructions and if mountains and tresors are interacting correctly with adventurers

        //Arrange

        //reference
        MapTempo reference = new MapTempo();
        reference.setWidth(3);
        reference.setHeight(4);

        List<int[]> mountainList = new ArrayList<>();
        mountainList.add(new int[]{
                1,1
        });
        reference.setMoutainsList(mountainList);

        List<Treasure> treasureList = new ArrayList<>();
        Treasure treasure = new Treasure();
        treasure.setCoordinate(new int[]{
                2,0
        });
        treasure.setQuantity(1);
        treasureList.add(treasure);
        reference.setTreasuresList(treasureList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurerTempo = new Adventurer();
        adventurerTempo.setCoordinate(new int[]{
                2,0
        });
        adventurerTempo.setName("Lara");
        adventurerTempo.setInstructions("AGA".toCharArray());
        adventurerTempo.setOrientation("E");
        adventurerTempo.setTreasure(1);
        adventurersList.add(adventurerTempo);
        reference.setAdventurersList(adventurersList);

        //parameter
        MapTempo mapTempo = initializeMapTempo();

        //Act
        // this block is a security to check the longest list of instructions for adventurers in order to manage if they don't have the same amount of instructions
        Adventurer adventurerWithLongestInstr = mapTempo.getAdventurersList().get(0);
        for (int i = 1; i < mapTempo.getAdventurersList().size(); i++){
            if (mapTempo.getAdventurersList().get(i).getInstructions().length > adventurerWithLongestInstr.getInstructions().length){
                adventurerWithLongestInstr = mapTempo.getAdventurersList().get(i);
            }
        }

        //in this block each adventurer will do one instruction at once
        int y = 0;
        while(y < adventurerWithLongestInstr.getInstructions().length){

            for (Adventurer adventurer : mapTempo.getAdventurersList()){

                if (adventurer.getInstructions().length > y){

                    switch (adventurer.getInstructions()[y]) {
                        case 'D' -> adventurer.turnToTheRight(); //if the instruction is to turn to the right
                        case 'G' -> adventurer.turnToTheLeft(); //if the instruction is to turn to the left
                        case 'A' -> mapTempo.moove(adventurer); //if the instruction is to moove forward
                    }
                }

            }
            y++;
        }

        //Assert
        assertThat(reference)
                .usingRecursiveComparison()
                .isEqualTo(mapTempo);

    }

    @Test
    void explorationWithTwoAdventurers() { //checks if the adventurers can moove on the same coordinates

        //Arrange

        //reference
        MapTempo reference = new MapTempo();
        reference.setWidth(3);
        reference.setHeight(4);

        List<int[]> mountainList = new ArrayList<>();
        mountainList.add(new int[]{
                0,0
        });
        reference.setMoutainsList(mountainList);

        List<Treasure> treasureList = new ArrayList<>();
        Treasure treasure = new Treasure();
        treasure.setCoordinate(new int[]{
                2,0
        });
        treasure.setQuantity(1);
        treasureList.add(treasure);
        reference.setTreasuresList(treasureList);

        List<Adventurer> adventurersList = new ArrayList<>();
        Adventurer adventurerTempo = new Adventurer();
        adventurerTempo.setCoordinate(new int[]{
                2,0
        });
        adventurerTempo.setName("Lara");
        adventurerTempo.setInstructions("AGA".toCharArray());
        adventurerTempo.setOrientation("E");
        adventurerTempo.setTreasure(1);
        adventurersList.add(adventurerTempo);
        adventurersList.add(new Adventurer("Ed", new int[]{1,1}, new char[]{'D'}, "E", 0));
        reference.setAdventurersList(adventurersList);

        //parameter
        MapTempo mapTempo = initializeMapTempo();

        List<int[]> mountains = new ArrayList<>();
        mountains.add(new int[]{0,0});
        mapTempo.setMoutainsList(mountains);

        Adventurer secondAdventurer = new Adventurer("Ed", new int[]{1,1}, new char[]{'D'}, "N", 0);
        mapTempo.getAdventurersList().add(secondAdventurer);

        //Act
        // this block is a security to check the longest list of instructions for adventurers in order to manage if they don't have the same amount of instructions
        Adventurer adventurerWithLongestInstr = mapTempo.getAdventurersList().get(0);
        for (int i = 1; i < mapTempo.getAdventurersList().size(); i++){
            if (mapTempo.getAdventurersList().get(i).getInstructions().length > adventurerWithLongestInstr.getInstructions().length){
                adventurerWithLongestInstr = mapTempo.getAdventurersList().get(i);
            }
        }

        //in this block each adventurer will do one instruction at once
        int y = 0;
        while(y < adventurerWithLongestInstr.getInstructions().length){

            for (Adventurer adventurer : mapTempo.getAdventurersList()){

                if (adventurer.getInstructions().length > y){

                    switch (adventurer.getInstructions()[y]) {
                        case 'D' -> adventurer.turnToTheRight(); //if the instruction is to turn to the right
                        case 'G' -> adventurer.turnToTheLeft(); //if the instruction is to turn to the left
                        case 'A' -> mapTempo.moove(adventurer); //if the instruction is to moove forward
                    }
                }

            }
            y++;
        }

        //Assert
        assertThat(reference)
                .usingRecursiveComparison()
                .isEqualTo(mapTempo);

    }
}