package Main;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MapTempo {

    private List<int[]> moutainsList = new ArrayList<>();
    private List<Treasure> treasuresList = new ArrayList<>();
    private List<Adventurer> adventurersList = new ArrayList<>();
    private int height;
    private int width;

    public MapTempo(List<int[]> moutainsList, List<Treasure> treasuresList, List<Adventurer> adventurersList, int height, int width, String originalLine) {
        this.moutainsList = moutainsList;
        this.treasuresList = treasuresList;
        this.adventurersList = adventurersList;
        this.height = height;
        this.width = width;
    }

    public MapTempo() {
    }

    public List<int[]> getMoutainsList() {
        return moutainsList;
    }

    public void setMoutainsList(List<int[]> moutainsList) {
        this.moutainsList = moutainsList;
    }

    public List<Treasure> getTreasuresList() {
        return treasuresList;
    }

    public void setTreasuresList(List<Treasure> treasuresList) {
        this.treasuresList = treasuresList;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public List<Adventurer> getAdventurersList() {
        return adventurersList;
    }

    public void setAdventurersList(List<Adventurer> adventurersList) {
        this.adventurersList = adventurersList;
    }

    public boolean isItAvailable(int[] coordinate){ //function that checks if an adventurer can moove to the coordinates in parameter

        if(coordinate[0] < 0 || coordinate[1] < 0){
            return false;
        }else if (coordinate[0] > this.width-1 || coordinate[1] > this.height-1){
            return false;
        }

        boolean flag = true;

        for (int[] tab : moutainsList){
            if (tab[0] == coordinate[0] && tab[1] == coordinate[1]) {
                flag = false;
                break;
            }
        }

        for (Adventurer tempo : adventurersList){
            if (tempo.getCoordinate()[0] == coordinate[0] && tempo.getCoordinate()[1] == coordinate[1]){
                flag = false;
                break;
            }
        }

        return flag;
    }

    public boolean isThereATreasure(int[] coordinate){ //if the adventurer can moove to coordinates we check if there is a treasure
        for (Treasure treasure : treasuresList){
            if (treasure.getCoordinate()[0] == coordinate[0]
                    && treasure.getCoordinate()[1] == coordinate[1]
                    && treasure.getQuantity() > 0){
                treasure.setQuantity(treasure.getQuantity()-1);
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    public void moove(Adventurer adventurer){ //function that manage the orientation of adventurers to make them moove in the right direction

        int[] tempo = new int[2];

        switch (adventurer.getOrientation()) {

            case "N" -> { //if adventurer is looking to the north
                tempo = new int[]{
                        adventurer.getCoordinate()[0],
                        adventurer.getCoordinate()[1] - 1,
                };
                if (isItAvailable(tempo)) adventurer.setCoordinate(tempo);
                if (isThereATreasure(tempo)) adventurer.setTreasure(adventurer.getTreasure() +1 );
            }

            case "S" -> { //if adventurer is looking to the south
                tempo = new int[]{
                        adventurer.getCoordinate()[0],
                        adventurer.getCoordinate()[1] + 1,
                };
                if (isItAvailable(tempo)) adventurer.setCoordinate(tempo);
                if (isThereATreasure(tempo)) adventurer.setTreasure(adventurer.getTreasure() +1 );
            }
            case "O" -> { //if adventurer is looking to the west
                tempo = new int[]{
                        adventurer.getCoordinate()[0] - 1,
                        adventurer.getCoordinate()[1],
                };
                if (isItAvailable(tempo)) adventurer.setCoordinate(tempo);
                if (isThereATreasure(tempo)) adventurer.setTreasure(adventurer.getTreasure() +1 );
            }

            case "E" -> { //if adventurer is looking to the east
                tempo = new int[]{
                        adventurer.getCoordinate()[0] + 1,
                        adventurer.getCoordinate()[1],
                };
                if (isItAvailable(tempo)) adventurer.setCoordinate(tempo);
                if (isThereATreasure(tempo)) adventurer.setTreasure(adventurer.getTreasure() +1 );
            }
        }
    }

    public List<String> exportMap(){ //function that get a list of string which correspond to what we are waiting for in the output file
        List<String> exportMap = new ArrayList<>();
        exportMap.add("C - " + this.width + " - " + this.height + System.lineSeparator());

        for (int[] mountain : moutainsList){
            exportMap.add("M - " + mountain[0] + " - " +
                    mountain[1] + System.lineSeparator());
        }

        for (Treasure treasure : treasuresList){
            if (treasure.getQuantity()!=0){
                exportMap.add("T - " + treasure.getCoordinate()[0] + " - " +
                        treasure.getCoordinate()[1] + " - " +
                        treasure.getQuantity() + System.lineSeparator());
            }

        }

        for (Adventurer adventurer : adventurersList){
            exportMap.add("A - " + adventurer.getName() + " - " +
                    adventurer.getCoordinate()[0] + " - " +
                    adventurer.getCoordinate()[1] + " - " +
                    adventurer.getOrientation() + " - " +
                    adventurer.getTreasure() + System.lineSeparator());
        }

        return exportMap;


    }

    @Override
    public String toString() {
        String stringToReturn = "C - " + this.width + " - " + this.height;

        for (int[] mountain : moutainsList){
            stringToReturn = stringToReturn + "M - " + mountain[0] + " - " +
                    mountain[1];
        }

        for (Treasure treasure : treasuresList){
            if (treasure.getQuantity()!=0){
                stringToReturn += "T - " + treasure.getCoordinate()[0] + " - " +
                        treasure.getCoordinate()[1] + " - " +
                        treasure.getQuantity();
            }

        }

        for (Adventurer adventurer : adventurersList){
            stringToReturn += "A - " + adventurer.getName() + " - " +
                    adventurer.getCoordinate()[0] + " - " +
                    adventurer.getCoordinate()[1] + " - " +
                    adventurer.getOrientation() + " - " +
                    adventurer.getTreasure();
        }

        return stringToReturn;

    }
}
