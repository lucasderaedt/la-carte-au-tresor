package Main;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> lines = WriteAndRead.read();
        MapTempo mapTempo = MapTempoFunction.mapTempoCreation(lines);
        WriteAndRead.write(MapTempoFunction.exploration(mapTempo).exportMap());
        //mapTempo = Main.MapTempoFunction.exploration(mapTempo);
        //Main.WriteAndRead.write(mapTempo.exportMap());
    }

}
