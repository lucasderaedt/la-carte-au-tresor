package Main;

import java.util.List;

public class MapTempoFunction {

    public static int[] getCoordinate(String[] line){ // a function that returns coordinates from a String[] in order to not do it several time below

        return new int[] {
                Integer.parseInt(line[1]),
                Integer.parseInt(line[2])
        };

    }

    public static MapTempo mapTempoCreation(List<String> lines){  // function to create a Main.MapTempo which is used to manage adventurers' exploration

        MapTempo mapTempo = new MapTempo();

        for (String line : lines){

            String[] tab = line.split(" - "); //splits the raw line to get only the values that are interesting for us

            switch (tab[0]) {
                case "C" -> { // if it's the map
                    mapTempo.setWidth(Integer.parseInt(tab[1]));
                    mapTempo.setHeight(Integer.parseInt(tab[2]));
                }

                case "M" -> { //if it's a mountain
                    mapTempo.getMoutainsList().add(getCoordinate(tab));
                }

                case "T" -> { //if it's a treasure
                    Treasure treasure = new Treasure();
                    treasure.setCoordinate(getCoordinate(tab));
                    treasure.setQuantity(Integer.parseInt(tab[3]));
                    mapTempo.getTreasuresList().add(treasure);
                }

                case "A" -> { //if it's an adventurer
                    Adventurer adventurer = new Adventurer();
                    adventurer.setCoordinate(new int[]{
                            Integer.parseInt(tab[2]),
                            Integer.parseInt(tab[3])
                    });
                    adventurer.setName(tab[1]);
                    adventurer.setOrientation(tab[4]);
                    adventurer.setInstructions(tab[5].toCharArray());
                    mapTempo.getAdventurersList().add(adventurer);
                }
            }

        }
        return mapTempo;
    }

    public static MapTempo exploration(MapTempo mapTempo){ //function that manage adventurers' instructions

        // this block is a security to check the longest list of instructions for adventurers in order to manage if they don't have the same amount of instructions
        Adventurer adventurerWithLongestInstr = mapTempo.getAdventurersList().get(0);
        for (int i = 1; i < mapTempo.getAdventurersList().size(); i++){
            if (mapTempo.getAdventurersList().get(i).getInstructions().length > adventurerWithLongestInstr.getInstructions().length){
                adventurerWithLongestInstr = mapTempo.getAdventurersList().get(i);
            }
        }

        //in this block each adventurer will do one instruction at once
        int y = 0;
        while(y < adventurerWithLongestInstr.getInstructions().length){

            for (Adventurer adventurer : mapTempo.getAdventurersList()){

                if (adventurer.getInstructions().length > y){

                    switch (adventurer.getInstructions()[y]) {
                        case 'D' -> adventurer.turnToTheRight(); //if the instruction is to turn to the right
                        case 'G' -> adventurer.turnToTheLeft(); //if the instruction is to turn to the left
                        case 'A' -> mapTempo.moove(adventurer); //if the instruction is to moove forward
                    }
                }

            }
            y++;
        }

        return mapTempo;

    }

}
