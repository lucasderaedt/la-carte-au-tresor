package Main;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WriteAndRead {

    public static List<String> read(){
        String file = "test.txt";
        List<String> lines = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(file)))
        {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        }
        catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return lines;
    }

    public static void write(List<String> lines){
        try {

            File file = new File("result.txt");

            // créer le fichier s'il n'existe pas
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            for (String line : lines){
                bw.write(line);
//                bw.write(System.lineSeparator());
            }
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
